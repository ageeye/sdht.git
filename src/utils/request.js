import axios from 'axios'
import { Cookies } from 'quasar'

/**
 * 获取请求的返回值
 * @param {Object} res
 * @returns {{data: *, status}}
 */
export function getResponse (res) {
  if (!res) {
    return {
      data: null,
      status: 500
    }
  }
  return {
    data: res.data.data || res.data,
    status: res.status
  }
}

/**
 * 处理异常
 * @param e
 * @returns {{data: *, status}}
 */
export function handleError (e) {
  // if (e.response.status === 401) {
  //   setTimeout(() => this.$store.commit('quit'), 0)
  // }
  return getResponse(e.response)
}

/**
 * 获取请求头
 * @param {Object} headers 自定义的头参数
 * @returns {Object}
 */
export function getHeaders (headers = {}) {
  if (process.env.SERVER) {
    return headers
  }
  // const token = Cookies.get('csrftoken')
  // if (token) {
  //   headers['X-CSRFToken'] = token
  // }
  return headers
}

/**
 * 处理API URL地址
 * @description 开发环境使用相对地址，线上环境使用https绝地地址
 * @param api
 * @returns {string|*}
 */
export function handleApiHost (api) {
  if (process.env.NODE_ENV === 'development' || api.match(/^https{0,1}:\/\//)) {
    return api
  } else {
    return `https://www.ditushu.com${api}`
  }
}

/**
 * 发送GET请求
 * @param {URL} api 相对地址或者绝对地址
 * @param {String} [id] 如果存在会替换api地址中的{id}
 * @param {Boolean} [loading] 是否改变当前vue组件data中的loading值
 * @param {String} [instance] 如果设置，请求成功后讲返回值写入vue组件data中对应的键中
 * @param {Object} [params] 额外的GET参数，如果有会自动添加到GET请求末尾
 * @returns {Promise<void>}
 */
export async function getRequest ({ api, id = null, type, loading = true, instance = 'instance', params = {} }) {
  const headers = getHeaders()
  api = handleApiHost(api)

  if (id) {
    api = api.replace('{id}', id)
  }

  if (type) {
    api = api.replace('{type}', type)
  }

  if (this && this.loading !== undefined && loading) {
    this.loading = true
  }

  try {
    const res = await axios.get(api, { headers, params })

    if (this) {
      this[instance] = res.data.data || res.data
    }

    return getResponse(res)
  } catch (e) {
    return handleError(e)
  } finally {
    if (this && this.loading !== undefined && loading) {
      this.loading = false
    }
  }
}

/**
 * 发送GET查询集请求
 * @description 这个主要用于查询列表数据，成功后会直接覆盖vue组件data中的querySet值
 * @param {URL} api 相对地址或者绝对地址
 * @param {Number} [id] 如果存在会替换api地址中的{id}
 * @param {Object} [params] 额外的GET参数，如果有会自动添加到GET请求末尾
 * @param {string} querySet 如果设置，请求成功后将数据列表返回值写入vue组件data中对应的键值
 * @param {string} count 如果设置，请求成功后将数据总数写入vue组件data中对应的键值
 * @param {string} mode - set|add|none 请求模式，set默认将覆盖vue组件data中的querySet,add则会添加到querySet数组末尾
 * @param {boolean} loading 是否改变当前vue组件data中的loading值
 * @param {function} callback 回掉函数，请求完成执行
 * @returns {Promise<void>}
 */
export async function getQuerySet ({
  api,
  id,
  type,
  page = 1,
  limit = 10,
  params = {},
  querySet = 'querySet',
  count = 'count',
  mode = 'set',
  callback,
  loading = true
}) {
  const headers = getHeaders()
  api = handleApiHost(api)

  if (id) {
    api = api.replace('{id}', id)
  }

  if (type) {
    api = api.replace('{type}', type)
  }

  if (page) {
    params.offset = (page - 1) * limit
  }

  if (limit) {
    params.limit = limit
  }

  if (this && this.loading !== undefined && loading) {
    this.loading = true
  }

  try {
    const res = await axios.get(api,
      { params, headers }
    )
    const data = res.data

    if (this) {
      if (mode === 'set' || !this[querySet]) {
        this[querySet] = data.results
        this[count] = data.count
      } else if (mode === 'add') {
        this[querySet] = this[querySet].concat(data.results)
        this[count] = data.count
      }

      if (this.nextPageQuery !== undefined) {
        this.nextPageQuery = data.next
      }
    }

    if (callback) {
      callback(data)
    }

    return getResponse(res)
  } catch (e) {
    return handleError(e)
  } finally {
    if (this && this.loading !== undefined && loading) {
      this.loading = false
    }
  }
}
