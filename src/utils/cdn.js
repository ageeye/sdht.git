async function loadRemoteJs (src) {
  return new Promise((resolve, reject) => {
    const element = document.createElement('script')

    element.type = 'text/javascript'
    element.onload = function () {
      resolve()
    }
    element.src = src
    document.body.appendChild(element)
  })
}

/**
 * 异步载入JS CDN资源库中的远程JS文件
 */
export async function loadCdnSources () {
  if (window.maptalks) {
    return false
  }
  const js = [
    'https://cdn.jsdelivr.net/npm/maptalks@1.0.0-beta.2/dist/maptalks.min.js',
    'https://cdn.jsdelivr.net/npm/@maptalks/gl@0.51.2/dist/maptalksgl.min.js',
    'https://cdn.jsdelivr.net/npm/@maptalks/vt@0.51.3/dist/maptalks.vt.min.js',
    'https://cdn.jsdelivr.net/npm/@maptalks/vt.basic@0.51.3/dist/maptalks.vt.basic.min.js'
  ]
  for (let i = 0; i < js.length; i++) {
    await loadRemoteJs(js[i])
  }
  return true
}
