const PointSymbol = {
  markerType: 'ellipse',
  markerFill: '#FFFF00',
  markerFillOpacity: 1,
  markerLineWidth: 3,
  markerWidth: 9,
  markerHeight: 9
}

const LineStringSymbol = {
  lineColor: '#1e88e5',
  lineWidth: 2,
  lineJoin: 'round',
  lineCap: 'round',
  lineOpacity: 0.9
}

const PolygonSymbol = {
  lineColor: '#1e88e5',
  lineWidth: 2,
  lineJoin: 'round',
  lineCap: 'round',
  lineOpacity: 0.9,
  polygonFill: '#1e88e5',
  polygonOpacity: 0.3
}

export default {
  Point: PointSymbol,
  MultiPoint: PointSymbol,
  LineString: LineStringSymbol,
  MultiLineString: LineStringSymbol,
  Polygon: PolygonSymbol,
  MultiPolygon: PolygonSymbol
}
