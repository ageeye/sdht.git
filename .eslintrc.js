module.exports = {
  root: true,

  parserOptions: {
    parser: '@babel/eslint-parser',
    sourceType: 'module'
  },

  env: {
    browser: true
  },

  extends: [
    'plugin:vue/vue3-recommended',
    'standard'
  ],

  // required to lint *.vue files
  plugins: [
    'vue'
  ],

  globals: {
    ga: 'readonly', // Google Analytics
    __statics: 'readonly',
    process: 'readonly',
    chrome: 'readonly',
    AMap: true,
    Loca: true,
    defineProps: 'readonly',
    defineEmits: 'readonly',
    defineExpose: 'readonly',
    withDefaults: 'readonly'
  },

  // add your custom rules here
  rules: {
    'no-console': 'off',
    'no-debugger': process.env.NODE_ENV === 'production' ? 'error' : 'off',

    'vue/max-attributes-per-line': [2, {
      singleline: 10,
      multiline: {
        max: 1,
        allowFirstLine: false
      }
    }],
    'vue/no-v-html': ['off'],
    'vue/singleline-html-element-content-newline': ['off'],
    'vue/component-tags-order': ['error', {
      order: ['script', 'template', 'style']
    }]
  }
}
